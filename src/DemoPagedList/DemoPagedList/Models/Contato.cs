﻿using System.Collections.Generic;

namespace DemoPagedList.Models
{
    public class Contato
    {
        public int IdContato { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }

        public List<Contato> GetAllContatos()
        {
            List<Contato> contatos = new List<Contato>();
            
            Contato c1 = new Contato 
            {
                IdContato = 1,
                Nome = "Jose",
                Email = "Jose@jose.com"
            };
            
            Contato c2 = new Contato
            {
                IdContato = 2,
                Nome = "Maria",
                Email = "maria@maria.com"
            };
            Contato c3 = new Contato
            {
                IdContato = 3,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            Contato c4 = new Contato
            {
                IdContato = 4,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            Contato c5 = new Contato
            {
                IdContato = 5,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            Contato c6 = new Contato
            {
                IdContato = 6,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            Contato c7 = new Contato
            {
                IdContato = 7,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            Contato c8 = new Contato
            {
                IdContato = 8,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            Contato c9 = new Contato
            {
                IdContato = 9,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            Contato c10 = new Contato
            {
                IdContato = 10,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            Contato c11 = new Contato
            {
                IdContato = 11,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            Contato c12 = new Contato
            {
                IdContato = 12,
                Nome = "Marcos",
                Email = "Marcos@Marcos.com"
            };
            contatos.Add(c1);
            contatos.Add(c2);
            contatos.Add(c3);
            contatos.Add(c4);
            contatos.Add(c5);
            contatos.Add(c6);
            contatos.Add(c7);
            contatos.Add(c8);
            contatos.Add(c9);
            contatos.Add(c10);
            contatos.Add(c11);
            contatos.Add(c12);


            return contatos;
        }
    }
}