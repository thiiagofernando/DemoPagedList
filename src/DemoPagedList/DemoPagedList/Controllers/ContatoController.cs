﻿using DemoPagedList.Models;
using System.Linq;
using System.Web.Mvc;
using PagedList;

namespace DemoPagedList.Controllers
{
    public class ContatoController : Controller
    {
        // GET: Contato
        public ActionResult Index(int? pagina)
        {
            var paginaAtual = pagina ?? 1;
            int TotalPorPagina = 5;
            Contato cont = new Contato();

            var lista = cont.GetAllContatos().OrderBy(t => t.IdContato)
                                              .ToPagedList(paginaAtual, TotalPorPagina);
            return View(lista);
        }
        
    }
}